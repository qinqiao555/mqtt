/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  mqtt_sub.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(05/08/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "05/08/23 16:06:48"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <mosquitto.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/time.h>
#include <string.h>

#include "log.h"
#include "cJSON.h"

#define ALIVE_Time 60

#define MQTT_SERVER "iot-06z00heqhhn3vco.mqtt.iothub.aliyuncs.com"
//#define MQTT_SERVER "127.0.0.1"
#define MQTT_PORT 	1883
#define MQTT_TOPIC "/k01jsVqbkLN/wendu/user/get"
#define clientId "k01jsVqbkLN.wendu|securemode=2,signmethod=hmacsha256,timestamp=1694337489385|"
#define username "wendu&k01jsVqbkLN"
#define password "6b469b5bb58f190aae5f9a373e84fe348e9d5bd708a7abb25df67df2036adf5a"

#if 0
#define MQTT_SERVER "b3579bbc9e.st1.iotda-device.cn-east-3.myhuaweicloud.com"
#define MQTT_PORT 1883
#define MQTT_TOPIC "$oc/devices/64fe78bc4f6df71a15f1396b_CurrentTemperature/sys/properties/report"
#define clientId "64fe78bc4f6df71a15f1396b_CurrentTemperature_0_0_2023091102"
#define username "64fe78bc4f6df71a15f1396b_CurrentTemperature"
#define password "d257e5054a1234b69cc06ee64774be59b67a31f3879d16234b26b4cfaea7936d"
#endif

void on_connect(struct mosquitto *mosq, void *obj, int rc);
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);

int main (int argc, char **argv)
{
	int								rc;
	int								loglevel=2;
	int								logsize=10;
	
	char							*logfile="sub.log";

	struct mosquitto                *mosq = NULL;

	/* init */
	if(log_init(logfile, loglevel, logsize) < 0)
    {
		fprintf(stderr, "initial logger system failure\n");
		return 1;
	}


	if( (mosquitto_lib_init()) < 0 )
	{
		LOG_WARN("mosquitto init failed\n");
	}
	else
	{
		LOG_INFO("mosquitto init successfully\n");
	}

	mosq = mosquitto_new(clientId, true, NULL);
	if(mosq == NULL)
	{
		LOG_WARN("new clientId error\n");
		mosquitto_lib_cleanup();
		return -1;
	}
	else
	{
		LOG_INFO("create mosquitto successfully\n");
	}

	if(mosquitto_username_pw_set(mosq, username, password) != MOSQ_ERR_SUCCESS )
	{
		LOG_WARN("mosq_connect() failed: %s\n", strerror(errno));
		mosquitto_destroy(mosq);
		mosquitto_lib_cleanup();
		return -2;
	}
	else
	{
		LOG_INFO("username_pw_set sucessfully!\n");
	}


	// 设置回调函数
	mosquitto_message_callback_set(mosq, on_message);
	mosquitto_connect_callback_set(mosq, on_connect);
	
	//建立连接
	rc = mosquitto_connect(mosq, MQTT_SERVER, MQTT_PORT, ALIVE_Time);
	if(rc != MOSQ_ERR_SUCCESS) 
	{
		LOG_WARN("Failed to connect to MQTT server, return code: %d\n", rc);
		mosquitto_destroy(mosq);
		return -3;
	}
	else
	{
		LOG_INFO("connect to MQTT server successfully\n");
	}

	// 进入事件循环
	//mosquitto_loop_forever(mosq, -1, 1);
	
#if 1
	// 循环处理消息
	while(1)
	{

		rc = mosquitto_loop(mosq, -1, 1);
		if(rc != MOSQ_ERR_SUCCESS) 
		{
			LOG_WARN("Mosquitto loop error: %d\n", rc);
			break;
		}
	}
#endif

	// 断开连接并清理资源
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();

	log_close();

	return 0;
} 

void on_connect(struct mosquitto *mosq, void *obj, int rc)
{
	if (rc == 0) 
	{
		LOG_INFO("Connected to MQTT broker\n");
		mosquitto_subscribe(mosq, NULL, MQTT_TOPIC, 0);
	}
	else
	{
		LOG_ERROR("Failed to connect to MQTT broker: %s\n", mosquitto_strerror(rc));
	}
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	cJSON				*root = cJSON_CreateObject();

	if(message->payload)
	{

		root = cJSON_Parse(message->payload);
		if(root == NULL)
		{
			LOG_ERROR("json pack into cjson error: %s\n", strerror(errno));
		}
		else
		{
			LOG_INFO("%s\n", cJSON_Print(root));
		}

		cJSON *color = cJSON_GetObjectItem(root, "color");
		char *led = color->valuestring;

		cJSON *state = cJSON_GetObjectItem(root, "state");
		char *status = state->valuestring;

		if( (strcmp(led, "red")) == 0 )
		{
			LOG_INFO("control red led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 13=0");
				LOG_INFO("red led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 13=1");
				LOG_INFO("red led off\n");
			}
		}

		if( (strcmp(led, "green")) == 0 )
		{
			LOG_INFO("control green led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 26=0");
				LOG_INFO("green led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 26=1");
				LOG_INFO("green led off\n");
			}
		}

		if( (strcmp(led, "blue")) == 0 )
		{
			LOG_INFO("control blue led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 19=0");
				LOG_INFO("blue led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 19=1");
				LOG_INFO("blue led off\n");
			}
		}

	}

	cJSON_Delete(root);
}

