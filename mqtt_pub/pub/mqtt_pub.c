/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  mqtt_pub.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(31/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "31/07/23 16:59:54"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <mosquitto.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/time.h>
#include <string.h>

#include "get_time.h"
#include "get_temp.h"
#include "para_json.h"
#include "log.h"

#define ALIVE_Time 60

void print_usage (char *progname);

#if 1
parameters          para={
	"iot-06z00heqhhn3vco.mqtt.iothub.aliyuncs.com",											//hostname
//	"127.0.0.1",
	"/sys/k01jsVqbkLN/wendu/thing/event/property/post", 									//topic
	"k01jsVqbkLN.wendu|securemode=2,signmethod=hmacsha256,timestamp=1690875119381|",		//clientId 
	"wendu&k01jsVqbkLN", 																	//username
	"c76d4699bf9452cea00e4197b74b58e4f8c07aed8c7cffc8f05c390ce12c2915",						//password 
	1883, 																					//port
	"{\"params\": {\"CurrentTemperature\": value}}"											//payload
};
#endif


int main (int argc, char **argv)
{
	int							time=0;
	int							count=0;
	int							ch;
	int							flag=1;
	int							sample=0;
	int							loglevel=2;
	int							logsize=10;

	char						datetime[32];
	char						*serv_ip;
	char						*logfile="pub.log";
	char						buf_temp[16];
	char						pub_buf[128];
	char						buf_send[32];

	float						temp;

	long						pack_time=0;
	long						current_time=0;

	struct mosquitto        	*mosq = NULL;
	struct hostent          	*hptr;
	struct in_addr           	myaddr;
	struct timeval				tloc;

	struct option				opts[]=
	{
		{"time", required_argument, NULL, 't'},
		{"Help", no_argument, NULL, 'H'},
		{NULL, 0, NULL, 0}
	};

	while((ch=getopt_long(argc, argv, "t:H", opts, NULL))!=-1)
	{
		switch(ch)
		{
			case 't':
				time=atoi(optarg);
				break;
			case 'H':
				print_usage(argv[0]);
				return 0;
		}
	}

	if(!time)
	{
		print_usage(argv[0]);
		return 0;
	}

	/* init */
	if(log_init(logfile, loglevel, logsize) < 0)
	{
		fprintf(stderr, "initial logger system failure\n");
		return 1;
	}


	if( (mosquitto_lib_init()) < 0 )
	{
		LOG_WARN("mosquitto init failed\n");
	}
	else
	{
		LOG_INFO("mosquitto init successfully\n");
	}


	/* NDS to IP */
	if((hptr = gethostbyname(para.hostname)) == NULL)
	{
		LOG_ERROR("DNS to IP failed: %s\n", strerror(errno));
		return -1;
	}
	memcpy(&myaddr.s_addr, hptr->h_addr, sizeof(hptr->h_addr));
	serv_ip = inet_ntoa(myaddr);
	//LOG_INFO("serv_ip: %s\n", serv_ip);

	mosq = mosquitto_new(para.clientId, true, NULL);
	if(mosq == NULL)
	{
		LOG_WARN("new clientId error\n");
		mosquitto_lib_cleanup();
		return -1;
	}
	else
	{
		LOG_INFO("create mosquitto successfully\n");
	}

	if(mosquitto_username_pw_set(mosq, para.username, para.password) != MOSQ_ERR_SUCCESS )
	{
		LOG_WARN("mosq_connect() failed: %s\n", strerror(errno));
		mosquitto_destroy(mosq);
		mosquitto_lib_cleanup();
		return -2;
	}
	else
	{
		LOG_INFO("username_pw_set sucessfully!\n");
	}

	if(mosquitto_connect(mosq, serv_ip, para.port, ALIVE_Time) != MOSQ_ERR_SUCCESS)
	{
		flag=0;

		LOG_WARN("mosq_connect() failed: %s\n", strerror(errno));
	}
	else
	{
		flag=1;

		LOG_INFO("connect [%s:%d] successfully\n", serv_ip, para.port);
	}


	while(1)
	{
		gettimeofday(&tloc, NULL);				
		current_time=tloc.tv_sec;

		if((current_time - pack_time) >= time)
		{
			sample=1;

			pack_time=get_time(datetime, sizeof(datetime));
			ds_temp(&temp);

			/* float type to char type */
			memset(buf_temp, 0, sizeof(buf_temp));
			snprintf(buf_temp, sizeof(buf_temp), "%.2f", temp);
		}
		else
		{
			sample=0;
		}


		if(flag)
		{
			if(1==sample)
			{
				memset(pub_buf, 0, sizeof(pub_buf));
				strncpy(pub_buf, para.payload, strlen(para.payload));

				/*  payload_get_temp() swap value and buf_temp */
				payload_get_temp(pub_buf, "value", buf_temp);

				if(mosquitto_publish(mosq, NULL, para.topic, strlen(pub_buf), pub_buf, 0, 0) != MOSQ_ERR_SUCCESS )      //QOS=0
				{
					LOG_ERROR("mosquitto_publish() failure: %s\n", strerror(errno));
				}
				else
				{
					LOG_INFO("publish information: %s\n", pub_buf);
				}
			}
		}

		sleep(5);
	} 

	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();

	log_close();

	return 0;
} 

void print_usage(char *progname)
{
	printf("%s usage: \n", progname);
	printf("-t(---time---): time interval for data reporting.\n");
	printf("-H(---Help---): print help information.\n");

	return ;
}


