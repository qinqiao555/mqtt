/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  log.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(04/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "04/07/23 16:37:45"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include "log.h"

#define data_size 1024

extern const char* __progname;
#define PROGRAM_NAME __progname

typedef struct log_s
{
	char		*file[32];
	FILE		*fp;
	int			 level;
	long		 size;
	int			 use_stdout;
} log_t;

static log_t g_logger;

static const char* LOG_LEVELS[] = {
	"ERROR",
	"WRAN",
	"INFO",
	"DEBUG"
};


int log_init(char *filename, int level, int size)
{
	FILE			*fp;

	log_term();

	g_logger.level=level;
	g_logger.size=size*1024;

	if(!filename || !strcmp(filename, "stderr"))
	{
		g_logger.use_stdout = 1;
		g_logger.fp = stderr;
		g_logger.size=0;
	}
	else
	{
		g_logger.use_stdout = 0;
		g_logger.fp = fopen(filename, "a+");
		if(!g_logger.fp)
		{
			fprintf(stderr, "Failed to open file \"%s\": %s", filename, strerror(errno));
			return -1;
		}
	}

	LOG_INFO("logger system start: file:\"%s\", level:%s, maxsize:%luKB\n\n",
			filename, LOG_LEVELS[level], size);

	return 0;
}


void log_term(void)
{
	if(!g_logger.fp)
	{
		return ;
	}

	if(!g_logger.use_stdout)
	{
		fclose(g_logger.fp);
	}

	g_logger.use_stdout = 0;
	g_logger.fp = NULL;

	return ;
}


/* write message to file */
void log_generic(const int level, const char* format, va_list args)
{
	time_t			tloc;
	struct tm		*ptr;
	char			datetime[32];
	char			data_message[data_size];

	time(&tloc);
	ptr=localtime(&tloc);
	snprintf(datetime, sizeof(datetime), "%04d-%02d-%02d %02d:%02d:%02d", 
											ptr->tm_year+1900,
											ptr->tm_mon+1,
											ptr->tm_mday,
											ptr->tm_hour,
											ptr->tm_min,
											ptr->tm_sec);
	vsprintf(data_message, format, args); 

	if(!g_logger.fp || level>g_logger.level)
		return ;

	fprintf(g_logger.fp, "%s: %s [%s] %s\n", PROGRAM_NAME, datetime, LOG_LEVELS[level], data_message);

	fflush(g_logger.fp);
}

void LOG_ERROR(char *format, ...)
{
	va_list			args;
	va_start(args, format);
	log_generic(LOG_LEVEL_ERROR, format, args);
	va_end(args);
}

void LOG_WARN(char *format, ...)
{
	if(g_logger.level < LOG_LEVEL_WARN)
	{
		return ;
	}

	va_list			args;
	va_start(args, format);
	log_generic(LOG_LEVEL_WARN, format, args);
	va_end(args);
}

void LOG_INFO(char *format, ...)
{
	if(g_logger.level < LOG_LEVEL_INFO)
	{
		return ;
	}

	va_list			args;
	va_start(args, format);
	log_generic(LOG_LEVEL_INFO, format, args);
	va_end(args);
}

void Log_DEBUG(char *format, ...)
{
	if(g_logger.level < LOG_LEVEL_DEBUG)
	{
		return ;
	}

	va_list			args;
	va_start(args, format);
	log_generic(LOG_LEVEL_DEBUG, format, args);
	va_end(args);
}


void log_close(void)
{
	if( g_logger.fp && g_logger.fp!=stderr )
	fclose(g_logger.fp);
}
