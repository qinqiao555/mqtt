/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  para_json.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(28/07/23)
 *         Author:  iot23<11>
 *      ChangeLog:  1, Release initial version on "28/07/23 17:33:05"
 *                 
 ********************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "para_json.h"


//modify the temperature in json paylaod
char *payload_get_temp(char *str, char *old, char *new)
{
	char	 *p = NULL;
	int 	  len = 0;
	char 	  newstr[200] = {0};

	p = strstr(str, old);			//p = payload中value及其之后的全部字符串
	len =  p - str;

	strncpy(newstr, str, len);		//把payload中value之前的字符全部复制到newstr
	strcat(newstr, new);			//把temperature拼接到newstr后
	strcat(newstr, p+strlen(old));	//把payload中value后面剩下的字符串也拼接到newstr
	strcpy(str, newstr);

	return str;
}

