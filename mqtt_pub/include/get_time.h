/********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  get_time.h
 *    Description:  This file 
 *
 *        Version:  1.0.0(03/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "03/07/23 12:59:21"
 *                 
 ********************************************************************************/

#ifndef GET_TIME_H
#define GET_TIME_H

#include <time.h>

int get_time(char *datetime, int datetime_len);

#endif
