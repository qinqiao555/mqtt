/********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  paraJson.h
 *    Description:  This file 
 *
 *        Version:  1.0.0(28/07/23)
 *         Author:  iot23<11>
 *      ChangeLog:  1, Release initial version on "28/07/23 18:34:35"
 *                 
 ********************************************************************************/

#ifndef PARA_JSON_H
#define PARA_JSON_H


typedef struct  
{
	char		hostname[80];
	char		topic[100];
	char		clientId[120];
	char		username[50];
	char		password[100];
	int			port;
	char		payload[100];
} parameters;

char *payload_get_temp(char *str, char *old, char *new);

#endif 
