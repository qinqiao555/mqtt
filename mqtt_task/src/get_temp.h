/********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  get_temp.h
 *    Description:  This file 
 *
 *        Version:  1.0.0(04/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "04/07/23 12:56:37"
 *                 
 ********************************************************************************/

#ifndef GET_TEMP_H
#define GET_TEMP_H

int ds_temp(float *temp);

#endif
