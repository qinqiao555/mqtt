/********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  log.h
 *    Description:  This file 
 *
 *        Version:  1.0.0(04/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "04/07/23 16:26:44"
 *                 
 ********************************************************************************/

#ifndef LOG_H
#define LOG_H

enum 
{
	LOG_LEVEL_ERROR,
	LOG_LEVEL_WARN,
	LOG_LEVEL_INFO,
	LOG_LEVEL_DEBUG,
	ALL,
};

int log_init(char *filename, int level, int size);
void log_term(void);
void log_generic(const int level, const char* format, va_list args);
void LOG_ERROR(char *format,...);
void LOG_WARN(char *format,...);
void LOG_INFO(char *format,...);
void LOG_DEBUG(char *format,...);
void log_close(void);

#endif
