/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  get_temp.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(04/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "04/07/23 11:17:19"
 *                 
 ********************************************************************************/

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  <dirent.h>
#include  <sys/types.h>
#include  <string.h>
#include  <time.h>
#include  <errno.h>
#include  <sys/stat.h>
#include  <fcntl.h>

#include  "log.h"
#include  "get_temp.h"

#define w1_path "/sys/bus/w1/devices/"
#define PATH_LENGTH 64

int ds_temp(float *temp)
{
	DIR    				*dirp=NULL;
	struct dirent 		*direntp=NULL;
	int    				 fd=-1;
	int    				 found=0;
	char   				*ptr=NULL;
	char   				 buf[128];
	char   				 chip[32];
	char   				 ds_path[PATH_LENGTH];

	if(( dirp = opendir(w1_path)) == NULL )
	{
		LOG_ERROR("Open %s failue:%s\n",w1_path,strerror(errno));
		return -1;
	}

	while( (direntp = readdir( dirp )) != NULL )
	{
		if( strstr(direntp->d_name,"28-"))
		{
			strncpy(chip,direntp->d_name,sizeof(chip));
			found=1;
			break;
		}
	}

	closedir(dirp);

	if(found != 1)
	{
		LOG_WARN("Can not find 28-041701d839ff in %s\n", w1_path);
		return -1;
	}

	snprintf(ds_path, PATH_LENGTH, "%s/%s/w1_slave", w1_path, chip);

	if( (fd = open(ds_path,O_RDONLY)) < 0 )
	{
		LOG_WARN("Open w1_slave failure:%s\n",strerror(errno));
		return -1;
	}

	memset(buf, 0, sizeof(buf));
	if( read(fd, buf, sizeof(buf)) < 0 )
	{
		LOG_WARN("Read w1_slave failure:%s:",strerror(errno));
		return -1;
	}

	ptr = strstr(buf,"t=");
	if(!ptr)
	{
		LOG_WARN("Does not include temperature in w1_slave\n");
		return -1;
	}
	ptr+=2;
	*temp = atof(ptr)/1000;
	
	close(fd);
	return 0;
}

