/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  get_time.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(03/07/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "03/07/23 12:49:25"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>

#include "get_time.h"

int get_time(char *datetime, int datetime_len)
{
	long				time;
	struct timeval		tloc;
	struct tm          *ptm;

	gettimeofday(&tloc, NULL);
	time=tloc.tv_sec;
	ptm=localtime(&tloc.tv_sec);

	memset(datetime, 0, sizeof(datetime));
	snprintf(datetime, 100, "%04d-%02d-%02d %02d:%02d:%02d",
			ptm->tm_year+1900, ptm->tm_mon+1, ptm->tm_mday,
			ptm->tm_hour, ptm->tm_min, ptm->tm_sec);

	return time;
}
