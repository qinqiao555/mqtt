/*********************************************************************************
 *      Copyright:  (C) 2023 iot23<11>
 *                  All rights reserved.
 *
 *       Filename:  main.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(22/12/23)
 *         Author:  iot23 <11>
 *      ChangeLog:  1, Release initial version on "22/12/23 22:57:01"
 *                 
 ********************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <mosquitto.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/time.h>
#include <string.h>

#include "get_time.h"
#include "get_temp.h"
#include "para_json.h"
#include "log.h"
#include "cJSON.h"

#define MQTT_HOSTNAME 			"main.iot-yun.club"
#define MQTT_PORT 				10883
#define MQTT_CLIENT_ID			"BearKE-0001"
#define MQTT_PUB_TOPIC 			"$Sys/Studio/Uplink/"MQTT_CLIENT_ID
#define MQTT_SUB_TOPIC			"$Sys/Studio/Downlink/"MQTT_CLIENT_ID
#define MQTT_USERNAME 			"lingyun"
#define MQTT_PASSWORD			"lingyun"
#define ALIVE_Time 				60

#define time_after(a,b)			( (int32_t)(b) - (int32_t)(a) < 0 )
#define time_before(a,b)		time_after(b,a)


void print_usage(char *progname);
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);


int main (int argc, char **argv)
{
	int							time=0;
	int							count=0;
	int							ch;
	int							flag=1;
	int							sample=0;
	int							loglevel=2;
	int							logsize=10;
	int							rv;

	char						datetime[32];
	char						*serv_ip;
	char						*logfile="task.log";
	char						buf_temp[16];
	char						pub_buf[64];
	char						payload[]="{\"params\": {\"CurrentTemperature\": value}}";

	float						temp;

	long						pack_time=0;
	long						current_time=0;

	struct mosquitto        	*mosq = NULL;
	struct timeval				tloc;

	struct option				opts[]=
	{
		{"time", required_argument, NULL, 't'},
		{"Help", no_argument, NULL, 'H'},
		{NULL, 0, NULL, 0}
	};

	while((ch=getopt_long(argc, argv, "t:H", opts, NULL))!=-1)
	{
		switch(ch)
		{
			case 't':
				time=atoi(optarg);
				break;
			case 'H':
				print_usage(argv[0]);
				return 0;
		}
	}

	if(!time)
	{
		print_usage(argv[0]);
		return -1;
	}

	/*  init */
	if(log_init(logfile, loglevel, logsize) < 0)
	{
		fprintf(stderr, "initial logger system failure\n");
		return -2;
	}

	if( (mosquitto_lib_init()) < 0 )
	{
		LOG_WARN("mosquitto init failed\n");
	}

	mosq = mosquitto_new(MQTT_CLIENT_ID, true, NULL);
	if(mosq == NULL)
	{
		LOG_WARN("new clientId error\n");
		mosquitto_lib_cleanup();
		return -3;
	}
	else
	{
		LOG_INFO("create mosquitto successfully\n");
	}

	if(mosquitto_username_pw_set(mosq, MQTT_USERNAME, MQTT_PASSWORD) != MOSQ_ERR_SUCCESS )
	{
		LOG_WARN("mosq_connect() failed: %s\n", strerror(errno));
		mosquitto_destroy(mosq);
		mosquitto_lib_cleanup();
		return -4;
	}

	if(mosquitto_connect(mosq, MQTT_HOSTNAME, MQTT_PORT, ALIVE_Time) != MOSQ_ERR_SUCCESS)
	{	
		flag=0;

		LOG_WARN("mosq_connect() failed: %s\n", strerror(errno));
	}
	else
	{
		flag=1;

		LOG_INFO("connect [%s:%d] successfully\n", MQTT_HOSTNAME, MQTT_PORT);
	}


	while(1)
	{
		gettimeofday(&tloc, NULL);				
		current_time=tloc.tv_sec;

		if((current_time - pack_time) >= time)
		{
			sample=1;

			ds_temp(&temp);
			/* float type to char type */
			memset(buf_temp, 0, sizeof(buf_temp));
			snprintf(buf_temp, sizeof(buf_temp), "%.2f", temp);

			pack_time=get_time(datetime, sizeof(datetime));
		}
		else
		{
			sample=0;
		}


		if(flag)	//connect successed
		{
			if(1==sample)
			{
				memset(pub_buf, 0, sizeof(pub_buf));
				strncpy(pub_buf, payload, strlen(payload));

				/* payload_get_temp() swap value and buf_temp */
				payload_get_temp(pub_buf, "value", buf_temp);


				/* publish */																											
				if(mosquitto_publish(mosq, NULL, MQTT_PUB_TOPIC, strlen(pub_buf), pub_buf, 0, 0) != MOSQ_ERR_SUCCESS )      //QOS=0
				{
					LOG_ERROR("mosquitto_publish() failure: %s\n", strerror(errno));
				}
				else
				{
					LOG_INFO("publish information: %s\n", pub_buf);
				}
			}

			/* subscribr */
			rv = mosquitto_loop(mosq, -1, 1);
			if(rv != MOSQ_ERR_SUCCESS) 
			{
				LOG_WARN("Mosquitto loop error, rv=%d\n", rv);
				break;
			}
		}
	}

	// 断开连接并清理资源
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();

	log_close();

	return 0;
} 


void print_usage(char *progname)
{
	printf("%s usage: \n", progname);
	printf("-t(---time---): time interval for data reporting.\n");
	printf("-H(---Help---): print help information.\n");

	return ;
}


void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	cJSON				*root = cJSON_CreateObject();

	if(message->payload)
	{

		root = cJSON_Parse(message->payload);
		if(root == NULL)
		{
			LOG_ERROR("json pack into cjson error: %s\n", strerror(errno));
		}
		else
		{
			LOG_INFO("%s\n", cJSON_Print(root));
		}

		cJSON *color = cJSON_GetObjectItem(root, "color");
		char *led = color->valuestring;

		cJSON *state = cJSON_GetObjectItem(root, "state");
		char *status = state->valuestring;

		if( (strcmp(led, "red")) == 0 )
		{
			LOG_INFO("control red led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 13=0");
				LOG_INFO("red led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 13=1");
				LOG_INFO("red led off\n");
			}
		}

		if( (strcmp(led, "green")) == 0 )
		{
			LOG_INFO("control green led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 26=0");
				LOG_INFO("green led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 26=1");
				LOG_INFO("green led off\n");
			}
		}

		if( (strcmp(led, "blue")) == 0 )
		{
			LOG_INFO("control blue led\n");

			if( strcasecmp(status, "on") == 0 )
			{
				system("sudo gpioset gpiochip0 19=0");
				LOG_INFO("blue led on\n");
			}
			if( strcasecmp(status, "off") == 0 )
			{
				system("sudo gpioset gpiochip0 19=1");
				LOG_INFO("blue led off\n");
			}
		}
	}

	cJSON_Delete(root);
}

